#!/usr/bin/python
"""
Copyright 2021 Ethan D. Monat
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import argparse
import json
import re
import datetime
# Define help and usage for monitoring-alert utility using argparse
parser = argparse.ArgumentParser(
    usage='''Usage: monitoring-alert.py [-h, --help] [filename]''',
    description='''monitoring-alert accepts an input file in a pipe-delimited format, and outputs alerts when batV is \
    below, or temp is above, allowable values for 5 minutes.''',
    epilog="""13APR21 Original Version by Ethan Monat, https://gitlab.com/likethepainter"""
)
parser.add_argument("filename", help="Satellite log file to parse", type=argparse.FileType('r'))
arg = parser.parse_args()

initListSat1000High = []
initListSat1001High = []
initListSat1000Low = []
initListSat1001Low = []
dataFileKeys = ["timestamp",
                "satelliteID",
                "red-high-limit",
                "yellow-high-limit",
                "yellow-low-limit",
                "red-low-limit",
                "raw-value",
                "component",
                "severity"]


def checkRedHigh(item):
    if float(item.split('|')[6]) > float(item.split('|')[2]):
        return True
    else:
        return False


def checkRedLow(item):
    if float(item.split('|')[6]) < float(item.split('|')[5]):
        return True
    else:
        return False


def timeComparison(item):
    # Guard against no values matching init dict
    if item:
        for dataPoints in item:
            itemValue = (dataPoints.split('|')[0].split()[1])
        timeDiff = []
        for inputValue in itemValue:
            (inputHours, inputMinutes, inputSeconds) = itemValue.split(':')
            diff = datetime.timedelta(hours=int(inputHours), minutes=int(inputMinutes), seconds=float(inputSeconds))
            timeDiff.append(diff)
        timeDiffValue = max(timeDiff) - min(timeDiff)
    else:
        exit()
    return timeDiffValue < datetime.timedelta(minutes=5)


def jsonDumping(item):
    # Getting late. Definitely a better way to do this. TODO: Fix this mess
    itemTime = item[:4] + "-" + item[4:6] + "-" + item[6:8] + "T" + re.split("[ |]", item)[1] + "Z" + "|" + item.split('|')[1] + "|" + item.split('|')[2] + "|" + item.split('|')[3] + "|" + item.split('|')[4] + "|" + item.split('|')[5] + "|" + item.split('|')[6] + "|" + item.split('|')[7] + "|" + item.split('|')[8]
    itemCsv = re.sub("[|]", ",", itemTime.rstrip())
    dataSplit = itemCsv.split(",")
    dataFileDict = dict(zip(dataFileKeys, dataSplit))
    dataFileDict.pop('yellow-high-limit')
    dataFileDict.pop('yellow-low-limit')
    dataFileDict.pop('red-high-limit')
    dataFileDict.pop('red-low-limit')
    dataFileDict.pop('raw-value')
    print(json.dumps(dataFileDict, indent=2))


# Filter out lines that don't contain values that exceed limits, sort into Sat-IDs lists
with open(arg.filename.name, 'r') as dataFile:
    for lineInFile in dataFile:
        if "1000" in lineInFile.split('|')[1]:
            if checkRedHigh(lineInFile) is True:
                strippedLineInFile = lineInFile.rstrip()
                initListSat1000High.append(strippedLineInFile + "|RED HIGH")
            elif checkRedLow(lineInFile) is True:
                strippedLineInFile = lineInFile.rstrip()
                initListSat1000Low.append(strippedLineInFile + "|RED LOW")
        elif "1001" in lineInFile.split('|')[1]:
            if checkRedHigh(lineInFile) is True:
                strippedLineInFile = lineInFile.rstrip()
                initListSat1001High.append(strippedLineInFile + "|RED HIGH")
            elif checkRedLow(lineInFile) is True:
                strippedLineInFile = lineInFile.rstrip()
                initListSat1001Low.append(strippedLineInFile + "|RED LOW")

if timeComparison(initListSat1000High) is True:
    jsonDumping(initListSat1000High[0])
if timeComparison(initListSat1000Low) is True:
    jsonDumping(initListSat1000Low[0])
if timeComparison(initListSat1001High) is True:
    jsonDumping(initListSat1001High[0])
if timeComparison(initListSat1001Low) is True:
    jsonDumping(initListSat1001Low[0])
